﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Models
{
    public class Station: BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Route> Routes { get; set; }

        public Station()
        {
            Routes = new List<Route>();
        }

        public void AddRouteTo(Station s , int distance)
        {
            Routes.Add(new Route() {
                From = this,
                To = s,
                Distance= distance
            });
        }
    }
}
