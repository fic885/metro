﻿namespace Metro.Models
{
    public class Route: BaseEntity
    {
        public virtual Station From { get; set; }
        public virtual Station To { get; set; }

        public int Distance { get; set; }
    }
}