﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.FileReadApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = "input.txt";
            if (args.Length > 0)
                file = args[0];
            new Program().Run(file);
            Console.ReadLine();
        }

        private void Run(string file)
        {
            File.ReadAllText(file)
                .Split(',')
                .Select(x=>x.Trim().Replace("-",":").Split(':'))
                .ToList()
                .ForEach(
                    x=> Console.WriteLine( 
                        string.Format("From {0} to {1} -> {2}",x[0],x[1],x[2])
                    )
                );

        }
    }
}
