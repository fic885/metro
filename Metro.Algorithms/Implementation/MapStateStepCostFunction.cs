﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Algorithms.Implementation
{
    public class MapStateStepCostFunction : StepCostFunction
    {
        public double calculateStepCost(object fromState, object toState, string action)
        {
            var from = (MapState) fromState;
            var to = (MapState) toState;

            var route = from.Map.First(x=> x.Item1==from.CurrentStation && x.Item2==to.CurrentStation);
            return route.Item3;
        }
    }
}
