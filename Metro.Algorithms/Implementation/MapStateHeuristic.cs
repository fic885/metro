﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Algorithms.Implementation
{
    public class MapStateHeuristic : HeuristicFunction
    {
        public int getHeuristicValue(object state)
        {
            var s = (MapState)state;
            return s.Heuristic();
        }
    }
}
