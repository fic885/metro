﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Algorithms.Implementation
{
    public class PathFinder
    {
        private int[] path;
        private int distance = 0;

        public PathFinder(MapState s)
        {
            Problem p = new Problem(
                    s,
                    new MapStateSuccessorFunction(),
                    new MapStateGoalTest(),
                    new MapStateStepCostFunction()
                );

            Search search = new AStarSearch(new TreeSearch());

            ArrayList solution = search.search(p);

            List<int> returnList = new List<int>();

            returnList.Add(s.CurrentStation);

            foreach (var x in solution)
            {
                returnList.Add(int.Parse(x.ToString()));
            }

            path = returnList.ToArray();

            distance = s.DistanceForPath(path);
        }

        public int[] GetPath()
        {
            return path;
        }
        public bool PathExists()
        {
            return path.Length > 1;
        }
        public int GetShortestDistance()
        {
            return distance;
        }
        public static int[] FindPath(MapState s)
        {
            Problem p = new Problem(
                    s,
                    new MapStateSuccessorFunction(),
                    new MapStateGoalTest(),
                    new MapStateStepCostFunction()
                );
            Search search = new AStarSearch(new TreeSearch());

            ArrayList solution = search.search(p);

            List<int> returnList = new List<int>();

            foreach(var x in solution)
            {
                returnList.Add(int.Parse(x.ToString()));
            }

            return returnList.ToArray();
        }

        


    }
}
