﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Algorithms.Implementation
{
    public class MapStateSuccessorFunction : SuccessorFunction
    {
        public ArrayList getSuccessors(object state)
        {
            var s = (MapState)state;
            return s.Successors();
        }
    }
}
