﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Algorithms.Implementation
{
    public class MapState: ICloneable
    {
        public List<Tuple<int,int,int>> Map { get; set; }
        public int CurrentStation { get; set; }

        public int EndStation { get; private set; }


        public MapState(List<Tuple<int,int,int>> Map , int start , int end)
        {
            this.Map = Map;
            CurrentStation = start;
            EndStation = end;
        }

        public bool IsGoal()
        {
            return CurrentStation == EndStation;
        }

        public int Heuristic()
        {
            return IsGoal() ? 0 : 1;
        }


        public void Update(int StationId)
        {
            CurrentStation = StationId;
        }


        public ArrayList Successors()
        {
            var list = new ArrayList();

            var neighbours = Map.Where(x => x.Item1 == CurrentStation);

            foreach(var x in neighbours)
            {
                var state = (MapState) Clone();
                state.Update(x.Item2);
                list.Add(new Successor(x.Item2.ToString() , state));
            }

            return list;
        }

        public int DistanceForPath(int[] path)
        {
            var distance = 0;
            for (int i = 0; i < path.Length - 1; i++)
            {
                var route = Map.First(x => x.Item1 == path[i] && x.Item2 == path[i + 1]);
                if (route == null) return -1;
                distance += route.Item3;
            }
            return distance;
        }

        public object Clone()
        {
            var newState = new MapState(Map, CurrentStation, EndStation);
            return newState;
        }
    }
}
