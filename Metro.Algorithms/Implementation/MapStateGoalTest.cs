﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Algorithms.Implementation
{
    class MapStateGoalTest : GoalTest
    {
        public bool isGoalState(object state)
        {
            var s = (MapState)state;
            return s.IsGoal();
        }
    }
}
