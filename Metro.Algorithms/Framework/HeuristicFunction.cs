using System;

namespace Metro.Algorithms
{
	/// <summary>
	/// Summary description for HeuristicFunction.
	/// </summary>
	public interface HeuristicFunction 
	{

		int getHeuristicValue(Object state);

	}
}
