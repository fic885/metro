using System;

namespace Metro.Algorithms
{
	/// <summary>
	/// Summary description for GoalTest.
	/// </summary>
	public interface GoalTest
	{
		bool isGoalState(Object state);
	}
}
