using System;
using System.Collections;

namespace Metro.Algorithms
{
	/// <summary>
	/// Summary description for Search.
	/// </summary>
	public interface Search 
	{
		ArrayList search(Problem p); //throws exception

		Metrics getMetrics();
	}
}
