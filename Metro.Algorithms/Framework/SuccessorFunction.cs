using System;
using System.Collections;

namespace Metro.Algorithms
{
	/// <summary>
	/// Summary description for SuccessorFunction.
	/// </summary>
	public interface SuccessorFunction 
	{

		ArrayList getSuccessors(Object state);

	}
}
