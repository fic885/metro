namespace Metro.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class all : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Routes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Distance = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Station_Id = c.Int(),
                        From_Id = c.Int(),
                        To_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stations", t => t.Station_Id)
                .ForeignKey("dbo.Stations", t => t.From_Id)
                .ForeignKey("dbo.Stations", t => t.To_Id)
                .Index(t => t.Station_Id)
                .Index(t => t.From_Id)
                .Index(t => t.To_Id);
            
            CreateTable(
                "dbo.Stations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Routes", "To_Id", "dbo.Stations");
            DropForeignKey("dbo.Routes", "From_Id", "dbo.Stations");
            DropForeignKey("dbo.Routes", "Station_Id", "dbo.Stations");
            DropIndex("dbo.Routes", new[] { "To_Id" });
            DropIndex("dbo.Routes", new[] { "From_Id" });
            DropIndex("dbo.Routes", new[] { "Station_Id" });
            DropTable("dbo.Stations");
            DropTable("dbo.Routes");
        }
    }
}
