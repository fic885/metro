namespace Metro.Data.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MetroEntites>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MetroEntites context)
        {
            var maksimir = new Station()
            {
                Name = "Maksimir"
            };

            var siget = new Station()
            {
                Name = "Siget"
            };

            var spansko = new Station()
            {
                Name = "Spansko"
            };

            var medvedscak = new Station()
            {
                Name = "Medvescak"
            };

            var dubrava = new Station()
            {
                Name = "Dubrava"
            };

            maksimir.AddRouteTo(siget, 5);
            siget.AddRouteTo(spansko, 4);
            spansko.AddRouteTo(medvedscak, 8);
            medvedscak.AddRouteTo(spansko, 8);
            medvedscak.AddRouteTo(dubrava, 6);
            maksimir.AddRouteTo(medvedscak, 5);
            spansko.AddRouteTo(dubrava, 2);
            dubrava.AddRouteTo(siget, 3);
            maksimir.AddRouteTo(dubrava, 7);

            context.Stations.AddOrUpdate(maksimir);
            context.Stations.AddOrUpdate(siget);
            context.Stations.AddOrUpdate(spansko);
            context.Stations.AddOrUpdate(medvedscak);
            context.Stations.AddOrUpdate(dubrava);

            context.Commit();
        }

    }
}
