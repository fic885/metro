﻿using Metro.Data.Configuration;
using Metro.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Data
{
    public class MetroEntites: DbContext
    {
        public MetroEntites() : base("MetroDb") { }

        public DbSet<Station> Stations { get; set; }
        public DbSet<Route> Routes { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StationConfiguration());
            modelBuilder.Configurations.Add(new RouteConfiguration());
        }
    }
}
