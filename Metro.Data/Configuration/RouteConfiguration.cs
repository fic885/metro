﻿using Metro.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Data.Configuration
{
    public class RouteConfiguration: EntityTypeConfiguration<Route>
    {
        public RouteConfiguration()
        {
            ToTable("Routes");
            Property(p => p.Distance).IsRequired();
            //HasRequired(p => p.From);
            //HasRequired(p => p.To);
        }
    }
}
