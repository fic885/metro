﻿using Metro.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Data.Configuration
{
    public class StationConfiguration : EntityTypeConfiguration<Station>
    {
        public StationConfiguration()
        {
            ToTable("Stations");

            Property(p => p.Name).IsRequired();
            //HasMany(p => p.Routes).WithRequired(p => p.From).WillCascadeOnDelete(false); 
            //HasMany(p => p.Routes).WithRequired(p => p.To).WillCascadeOnDelete(false); 

        }
    }
}
