﻿using Metro.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        MetroEntites dbContext;

        public MetroEntites Init()
        {
            return dbContext ?? (dbContext = new MetroEntites());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
