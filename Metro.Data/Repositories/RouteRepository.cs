﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metro.Data.Infrastructure;
using Metro.Models;

namespace Metro.Data.Repositories
{
    public class RouteRepository: RepositoryBase<Route>, IRouteRepository
    {
        public RouteRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
    public interface IRouteRepository: IRepository<Route>
    {

    }
}
