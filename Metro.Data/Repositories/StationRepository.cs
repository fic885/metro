﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metro.Data.Infrastructure;
using Metro.Models;

namespace Metro.Data.Repositories
{
    public class StationRepository: RepositoryBase<Station> , IStationRepository
    {
        public StationRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public Station GetStationByName(string name)
        {
            var station = DbContext.Stations.Where(x => x.Name.ToLower() == name.ToLower()).FirstOrDefault();
            return station;
        }
    }
    public interface IStationRepository: IRepository<Station>
    {
        Station GetStationByName(string name);
    }
}
