﻿using Metro.Algorithms.Implementation;
using Metro.Data.Infrastructure;
using Metro.Data.Repositories;
using Metro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Service
{
    public interface IRouteService
    {
        IEnumerable<Route> GetAllRoutes();
        IEnumerable<Route> GetRoutesFromStation(Station station);
        IEnumerable<Route> GetRoutesToStation(Station station);
        void CreateRoute(Route route);
        void SaveRoute();
        int GetShortestDistance(int from, int to);
        int[] GetShortestPath(int from, int to);
    }

    public class RouteService : IRouteService
    {
        private readonly IRouteRepository routeRepository;
        private readonly IUnitOfWork unitOfWork;

        public RouteService(IRouteRepository routeRepository, IUnitOfWork unitOfWork)
        {
            this.routeRepository = routeRepository;
            this.unitOfWork = unitOfWork;
        }

        public void CreateRoute(Route route)
        {
            routeRepository.Add(route);
        }

        public IEnumerable<Route> GetAllRoutes()
        {
            return routeRepository.GetAll();
        }

        public IEnumerable<Route> GetRoutesFromStation(Station station)
        {
            return routeRepository.GetMany(x => x.From.Id == station.Id);
        }
        public Route FindRoute(Station from, Station to)
        {
            var route = routeRepository.GetMany(x => x.From.Id == from.Id && x.To.Id == to.Id).FirstOrDefault();
            return route;
        }

        public IEnumerable<Route> GetRoutesToStation(Station station)
        {
            return routeRepository.GetMany(x => x.To.Id == station.Id);
        }

        

        public void SaveRoute()
        {
            unitOfWork.Commit();
        }

        public int GetShortestDistance(int from, int to)
        {
            var map = routeRepository
                .GetAll()
                .Select(x => new Tuple<int, int, int>(x.From.Id, x.To.Id, x.Distance))
                .ToList();

            var state = new MapState(map, from, to);
            var pf = new PathFinder(state);

            var distance = pf.GetShortestDistance();
            return distance;
        }

        public int[] GetShortestPath(int from, int to)
        {
            var map = routeRepository
                .GetAll()
                .Select(x => new Tuple<int, int, int>(x.From.Id, x.To.Id, x.Distance))
                .ToList();

            var state = new MapState(map, from, to);
            var pf = new PathFinder(state);

            return pf.GetPath();
        }

    }
}
