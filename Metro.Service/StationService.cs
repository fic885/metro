﻿using Metro.Algorithms.Implementation;
using Metro.Data.Infrastructure;
using Metro.Data.Repositories;
using Metro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metro.Service
{
    public interface IStationService
    {
        Station GetStation(int id);
        Station GetStationByName(string name);
        IEnumerable<Station> GetAllStations();
        int GetDistance(string[] stations);
        void CreateStation(Station station);
        void SaveStation();
    }

    public class StationService : IStationService
    {
        private readonly IStationRepository stationRepository;
        private readonly IUnitOfWork unitOfWork;

        public StationService(IStationRepository stationRepository, IUnitOfWork unitOfWork)
        {
            this.stationRepository = stationRepository;
            this.unitOfWork = unitOfWork;
        }

        public void CreateStation(Station station)
        {
            stationRepository.Add(station);
        }

        public IEnumerable<Station> GetAllStations()
        {
            return stationRepository.GetAll();
        }

        public int GetDistance(string[] stations)
        {
            var path = stationRepository
                .GetMany(x => stations.Contains(x.Name))
                .Select(x=>x.Id)
                .ToArray()
                ;

            if (path.Length != stations.Length) return -1;

            var map = stationRepository.GetAll()
                .SelectMany(x => x.Routes)
                .Distinct()
                .Select(x => new Tuple<int, int, int>(x.From.Id, x.To.Id, x.Distance))
                .ToList();
            ;

            var state = new MapState(map,path.First(),path.Last());
            var distance = state.DistanceForPath(path);
            return distance;
        }

        public Station GetStation(int id)
        {
            return stationRepository.GetById(id);
        }

        public Station GetStationByName(string name)
        {
            return stationRepository.GetStationByName(name);
        }

        public void SaveStation()
        {
            unitOfWork.Commit();
        }
    }
}
