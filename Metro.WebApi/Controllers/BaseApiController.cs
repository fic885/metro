﻿using Metro.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Metro.WebApi.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected readonly IStationService stationService;
        protected readonly IRouteService routeService;

        public BaseApiController(IStationService stationService, IRouteService routeService)
        {
            this.stationService = stationService;
            this.routeService = routeService;
        }
    }
}
