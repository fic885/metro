﻿using Metro.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Metro.WebApi.Controllers
{
    public class StationController : BaseApiController
    {
        public StationController(IStationService stationService, IRouteService routeService) : base(stationService, routeService)
        {
        }

        [Route("zagreb-metro/trip")]
        public string[] Get()
        {
            return stationService.GetAllStations().Select(x => x.Name).ToArray();
        }

        [Route("zagreb-metro/trip/{S}/routes")]
        public string[] GetRouteFromStation(string S)
        {
            var station = stationService.GetStationByName(S);

            return station.Routes.Select(x => x.To.Name).ToArray();
        }
    }
}
