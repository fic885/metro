﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Metro.Service;
using System.Web.Helpers;
using Metro.WebApi.Models;
using Metro.Models;

namespace Metro.WebApi.Controllers
{
    public class RouteController : BaseApiController
    {
        public RouteController(IStationService stationService, IRouteService routeService) : base(stationService, routeService)
        {
        }

        [Route("zagreb-metro/trip/distance")]
        public DistanceResponse GetDistance(DistanceRequest req)
        {
            var response = new DistanceResponse();
            response.distance = "NO SUCH ROUTE";
            if (req == null)
            {
                
                return response;
            }

            var distance = stationService.GetDistance(req.stations);
            if(distance>-1)
            {
                response.distance =distance.ToString();
                return response;
            }
            return response;
        }

        [Route("zagreb-metro/trip/shortest")]
        public DistanceResponse GetShortestDistance(ShortestDistanceRequest req)
        {
            var from = stationService.GetStationByName(req.stations.start);

            var to = stationService.GetStationByName(req.stations.end);

            var distance = routeService.GetShortestDistance(from.Id, to.Id);

            var res = new DistanceResponse();

            res.distance = distance.ToString();

            return res;
        }

        [Route("zagreb-metro/trip/shortestpath")]
        public string[] GetShortestPath(ShortestDistanceRequest req)
        {
            var from = stationService.GetStationByName(req.stations.start);

            var to = stationService.GetStationByName(req.stations.end);

            var path = routeService.GetShortestPath(from.Id, to.Id);
            var stations = stationService.GetAllStations();
            var query = path.Join(stations,
                p => p,
                s => s.Id,
                (p, s) => new { p, s.Name }
                )
                .Select(x => x.Name)
                .ToArray()
                ;
            return query;
        }

    }
}
