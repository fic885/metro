﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metro.WebApi.Models
{
    public class DistanceRequest
    {
        public string[] stations { get; set; }
    }
    public class DistanceResponse
    {
        public string distance { get; set; }
    }

    public class ShortestDistanceRequest
    {
        public RouteStations stations { get; set; }
    }

    public class RouteStations
    {
        public string start { get; set; }
        public string end { get; set; }

        public RouteStations(string start, string end)
        {
            this.start = start;
            this.end = end;
        }
    }
}