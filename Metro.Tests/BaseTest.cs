﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metro.Data.Infrastructure;
using Metro.Data.Repositories;
using Metro.Service;
using Autofac;
using System.Reflection;

namespace Metro.Tests
{
    public class BaseTest
    {
        protected IStationService stationService;
        protected IRouteService routeService;


        public BaseTest()
        {
            var builder = new ContainerBuilder();

            var models = Assembly.Load("Metro.Models");
            var data = Assembly.Load("Metro.Data");
            var service = Assembly.Load("Metro.Service");

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<DbFactory>().As<IDbFactory>();

            // Repositories
            builder.RegisterAssemblyTypes(data)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces();

            // Services
            builder.RegisterAssemblyTypes(service)
               .Where(t => t.Name.EndsWith("Service"))
               .AsImplementedInterfaces();

            var container = builder.Build();

            stationService = container.Resolve<IStationService>();
            routeService = container.Resolve<IRouteService>();
        }
    }
}
