﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Metro.Algorithms.Implementation;
using System.Collections.Generic;
using System.Linq;

namespace Metro.Tests
{
    [TestClass]
    public class AlgorithmsTests
    {
        [TestMethod]
        public void TestPathExists()
        {
            var path = new PathFinder(getMapState(1,5));

            Assert.IsTrue(path.PathExists());

        }

        [TestMethod]
        public void TestPathNotExists()
        {
            var path = new PathFinder(getMapState(1, 7));

            Assert.IsTrue(!path.PathExists());

        }

        [TestMethod]
        public void TestShortestPath()
        {
            var path = new PathFinder(getMapState(1, 5));
            var correctPath = new int[] {1, 3, 5 };

            Assert.IsTrue( Enumerable.SequenceEqual(path.GetPath(),correctPath) );
        }

        [TestMethod]
        public void TestDistance()
        {
            var path = new PathFinder(getMapState(1, 5));
            var distance = path.GetShortestDistance();
            Assert.AreEqual(3, distance);
        }

        private MapState getMapState(int start,int end)
        {
            var map = new List<Tuple<int, int, int>>();

            map.Add(new Tuple<int, int, int>(1, 2, 1));
            map.Add(new Tuple<int, int, int>(1, 3, 1));
            map.Add(new Tuple<int, int, int>(2, 4, 1));
            map.Add(new Tuple<int, int, int>(3, 5, 2));
            map.Add(new Tuple<int, int, int>(4, 5, 3));
            map.Add(new Tuple<int, int, int>(6, 7, 3));

            var state = new MapState(map, start, end);

            return state;
        }
    }
}
