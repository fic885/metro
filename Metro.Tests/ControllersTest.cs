﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Metro.WebApi.Controllers;
using Metro.WebApi.Models;
using Metro.Service;
using Moq;
using Metro.Data.Infrastructure;
using Metro.Data.Repositories;
using Autofac;
using System.Collections.Generic;
using System.Linq;

namespace Metro.Tests
{
    [TestClass]
    public class ControllersTest:BaseTest
    {

        [TestMethod]
        public void TestRepository()
        {
            Assert.IsTrue(stationService.GetAllStations().Count()==5);
        }

        [TestMethod]
        public void TestStations()
        {
            var controller = new StationController(stationService, routeService);
            string[] result = controller.Get();
            Assert.AreEqual(5, result.Length);

        }

        //Metro-2
        [TestMethod]
        public void TestControllerDistance()
        {
            var req = new DistanceRequest();
            req.stations = new string[] { "Maksimir" , "Siget" , "Spansko"};
            var controller = new RouteController(stationService, routeService);
            DistanceResponse result = controller.GetDistance(req);
            Assert.AreEqual("9", result.distance);
        }

        //Metro-5
        [TestMethod]
        public void TestShortestDistance()
        {
            var req = new ShortestDistanceRequest();
            req.stations = new RouteStations("Maksimir", "Spansko");

            var controller = new RouteController(stationService, routeService);
            DistanceResponse result = controller.GetShortestDistance(req);

            Assert.AreEqual("9", result.distance);

        }

        //Metro-Bonus
        [TestMethod]
        public void TestShortestPath()
        {
            var req =new ShortestDistanceRequest();// It's the same structure and thats why i use this request
            req.stations = new RouteStations("Maksimir", "Spansko");
            var controller = new RouteController(stationService, routeService);
            var response = controller.GetShortestPath(req);

            string[] correctPath = new string[] {"Maksimir", "Siget","Spansko" };
            Assert.IsTrue(Enumerable.SequenceEqual(correctPath, response));

        }

        
    }
}
